<?php

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

//todo: add your code
$cliRouter=new \QI\SimpleMvcBlog\Router\Router($argv);
$app = new \QI\SimpleMvcBlog\App($cliRouter);
$app->run();