<?php
namespace QI\SimpleMvcBlog;
use QI\SimpleMvcBlog\Model\Post;
use QI\SimpleMvcBlog\View\Error;
class App
{
    private $router;

    public function __construct($router){
        $this->router = $router;
    }
    public function run()
    {
        $defaultMethod = 'index';
        $defaultControllerName = "QI\SimpleMvcBlog\Controller\Post";

        if ($this->router->getController() == null){
            $defaultController = new $defaultControllerName();
            $defaultView = $defaultController->$defaultMethod();
            die($defaultView->render());
        }

        $controllerName = "QI\SimpleMvcBlog\Controller\\".ucfirst($this->router->getController());
        $interfaceName = 'QI\SimpleMvcBlog\Controller\ControllerInterface';

        if (!class_exists($controllerName)) {
            $this->displayError("Controller does not exists");
        }

        $controller = new $controllerName();
        if (!($controller instanceof $interfaceName)){
            $this->displayError("Controller does not implements ControllerInterface");
        }

        $method = $this->router->getAction() ?? $defaultMethod;
        $args = $this->router->getParameters();

        if (!method_exists($controller, $method)){
            $this->displayError("Method does not exists");
        }

        die($controller->$method($args)->render());

    }

    private function displayError(){
        $view = new Error();
        die($view->render());
    }
}