<?php
namespace QI\SimpleMvcBlog\Controller;
use QI\SimpleMvcBlog\Storage\HardCodePost;
use QI\SimpleMvcBlog\View\Posts;
use QI\SimpleMvcBlog\View\ViewInterface;
use QI\SimpleMvcBlog\Model;
use QI\SimpleMvcBlog\View\Error;
class Post implements ControllerInterface{
    private $model;
    public function __construct(){
        $this->model=new HardCodePost();
    }
    public function add($posts)
    {

        for ($i = 0; $i < count($posts); $i += 2) {
            $post = new Model\Post($posts[$i], $posts[$i + 1]);
            $addedPosts[] = $post;
            $this->model->add($post);
        }
        return new Posts("Posts added:", $addedPosts);
    }
    public function show($postsId){
        if(isset($this->posts[$postsId])){
            $posts[]=$this->model->search($this->posts[$postsId]);
            return $this->show(new Posts("Posts:",$posts));
        }
        return 'Post with this id not found';

    }

    public function index(): ViewInterface
    {
        return new Posts('Posts',$this->model->all());
    }
}