<?php

declare(strict_types=1);


namespace QI\SimpleMvcBlog\Model;


class Post
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $body;

    public function __construct(string $title, string $body)
    {
        $this->title = $title;
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }
}