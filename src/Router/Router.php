<?php
namespace QI\SimpleMvcBlog\Router;
    class Router
    {
        private $controller = null;
        private $action = null;
        private $parameters = null;

        public function __construct($args){
            if (isset($args[1])){
                $this->controller = $args[1];
            }

            if (isset($args[2])){
                if (is_numeric($args[2])){
                    $this->action = "show";
                    $this->parameters = array_slice($args, 2);
                }
                else{
                    $this->action = $args[2];
                    $this->parameters = array_slice($args, 3);
                }
            }

        }

        public function getController(){
            return $this->controller;
        }
        public function getAction(){
            return $this->action;
        }
        public function getParameters(): ?array{
            return $this->parameters;
        }
    }