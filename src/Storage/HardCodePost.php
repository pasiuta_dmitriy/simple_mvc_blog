<?php

declare(strict_types=1);


namespace QI\SimpleMvcBlog\Storage;


use QI\SimpleMvcBlog\Model\Post;
use QI\SimpleMvcBlog\Storage\Exception\NotFound;

class HardCodePost implements PostInterface
{
    private $posts = [
        ['first post title', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel consequat ipsum, vel porttitor tortor.']
    ];


    public function add(Post $post): void
    {
        $this->posts[] = [
            $post->getTitle(),
            $post->getBody()
        ];
    }

    public function search(int $id): Post
    {
        if (!array_key_exists($id, $this->posts)) {
            throw new NotFound("Post with id $id not found");
        }
        return new Post(
            $this->posts[$id][0],
            $this->posts[$id][1]
        );
    }

    /**
     * @inheritDoc
     */
    public function all(): array
    {
        return array_map(
            function (array $post) {
                return new Post(
                    $post[0],
                    $post[1]
                );
            },
            $this->posts
        );
    }
}