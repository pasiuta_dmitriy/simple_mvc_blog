<?php

declare(strict_types=1);


namespace QI\SimpleMvcBlog\Storage;


use QI\SimpleMvcBlog\Model\Post;
use QI\SimpleMvcBlog\Storage\Exception\NotFound;

interface PostInterface
{
    public function add(Post $post): void;

    /**
     * @param int $id
     *
     * @return Post
     *
     * @throws NotFound
     */
    public function search(int $id): Post;

    /**
     * @return Post[]
     */
    public function all(): array;
}