<?php
namespace QI\SimpleMvcBlog\View;


class Error implements ViewInterface
{
    public function render(): string{
        return 'error';
    }

}