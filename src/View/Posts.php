<?php
namespace QI\SimpleMvcBlog\View;
class Posts implements ViewInterface{

    private $output;
    private $posts;
    public function __construct($output, $posts){
        $this->output = $output;
        $this->posts = $posts;
    }

    public function render(): string
    {
        foreach ($this->posts as $post){
        $this->output=$post->getTitle(). " ". $post->getBody();
        }
        return $this->output;

    }
}