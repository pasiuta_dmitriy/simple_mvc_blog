<?php
use PHPUnit\Framework\TestCase;
use QI\SimpleMvcBlog\Router\Router;
class RouterTest extends TestCase
{
    //private $router;

    public function argsProvider(): array
    {
        return [

            [['cli.php', 'post'],
                ['controller' => 'post', 'action' => null, 'parameters' =>null ]],
            [['cli.php', 'post', '1'],
                ['controller' => 'post', 'action' => null, 'parameters' =>['1']]],

            [['cli.php', 'post', 'add', 'title', 'body'],
                ['controller' => 'post', 'action' => 'add', 'parameters' => ['title', 'body']]],

        ];
    }
    /**
     * @dataProvider argsProvider

     */
    public function testConstructor($args, $expected){

        $instance=new Router($args);
        $this->assertEquals($expected['controller'],$instance->getController());
        $this->assertEquals($expected['action'],$instance->getAction());
        $this->assertEquals($expected['parameters'],$instance->getParameters());
    }
}